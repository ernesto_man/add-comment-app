$(document).ready(function(){
   $.ajax({
	  type: 'GET',
	  dataType: 'JSON',
	  url: 'http://127.0.0.1:5000/get_comments',
	  success: function(data){
		console.log(data);
		data.forEach(function(item) {
			var id = item.id;
			var name = item.name;
			var email = item.email;
			var comment = item.comment;
			render_card(id, name, email, comment);
		});
		
	  }
	});
	
	
	
	$( "#form" ).submit(function( event ) {
		event.preventDefault();
		var data   = $('#form').serialize();
		console.log(data);
		$.ajax({
		  type: 'POST',
		  data: data,
		  dataType: 'JSON',
		  url: 'http://127.0.0.1:5000/write_comment',
		  success: function(data){
			var id = data.id;
			var name = data.name;
			var email = data.email;
			var comment = data.comment;
			render_card(id, name, email, comment)
		  }
		});
	});
	
function render_card(id, name, email, comment) {
	var $root = $( "#root" )
	str = "<span>id: " + id + "</span><br/>" + "<span>name: " + name + "</span><br/>" + "<span>email: " + email + "</span><br/>" + "<span>comment: " + comment + "</span><br/>";
	html = $.parseHTML( str );
	$root.append( html );
}

});