# -*- coding: utf-8 -*-
from flask import Flask, jsonify, request
from flask_cors import CORS
from flask.ext.mysql import MySQL
from flask import Response
import json

app = Flask(__name__)
CORS(app, supports_credentials=True)

mysql = MySQL()

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'kesha123'
app.config['MYSQL_DATABASE_DB'] = 'honeyhunters'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

@app.route('/get_comments', methods=['GET'])
def get_comments():
    conn = mysql.connect()
    cur = conn.cursor()
    
    query = "SELECT * FROM comments "
    row = cur.execute(query)
    
    comm = cur.fetchall()
    comments_list = []
    
    for comment in comm:
        comment_elem = {}
        comment_elem['id'] = comment[0]
        comment_elem['name'] = comment[1]
        comment_elem['email'] = comment[2]
        comment_elem['comment'] = comment[3]
        comments_list.append(comment_elem)
    
    
    conn.close()
    cur.close()
    return  json.dumps(comments_list)
    
@app.route('/write_comment', methods=['POST'])
def write_comment():
    name = request.form['name']
    email = request.form['email']
    comment = request.form['text']
    return write_comment_to_db(name, email, comment)
    
def write_comment_to_db(name, email, text):
    conn = mysql.connect()
    cur = conn.cursor()
    query = "INSERT INTO comments (name, email, text) VALUES ('" + name + "', '" + email + "','" + text + "')"
    row = cur.execute(query)
    conn.commit()
    query = "SELECT * from comments WHERE id = (SELECT max(id) FROM comments)"
    row = cur.execute(query)
    
    comments = cur.fetchall()
    comments_list = {}
    
    print comments
    
    for comment in comments:
        comments_list['id'] = comment[0]
        comments_list['name'] = comment[1]
        comments_list['email'] = comment[2]
        comments_list['comment'] = comment[3]
    
    
    conn.close()
    cur.close()
    return  json.dumps(comments_list)
    
if __name__ == "__main__":
    app.run(debug=True, threaded=True, host='0.0.0.0')