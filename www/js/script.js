$(document).ready(function(){
   $.ajax({
	  type: 'GET',
	  dataType: 'JSON',
	  url: 'http://127.0.0.1:5000/get_comments',
	  success: function(data){
		console.log(data);
		var comment_count = 0;
		data.forEach(function(item) {
			comment_count += 1;
			var id = item.id;
			var name = item.name;
			var email = item.email;
			var comment = item.comment;
			render_card(comment_count, name, email, comment);
		});
		
	  }
	});
	
	
	
	$( "#form" ).submit(function( event ) {
		event.preventDefault();
		var data   = $('#form').serialize();
		console.log(data);
		$.ajax({
		  type: 'POST',
		  data: data,
		  dataType: 'JSON',
		  url: 'http://127.0.0.1:5000/write_comment',
		  success: function(data){
			var id = data.id;
			var name = data.name;
			var email = data.email;
			var comment = data.comment;
			render_card(id, name, email, comment)
		  }
		});
	});
	
function render_card(id, name, email, comment) {
	//добавить возможность выбора стиля карты в зависимости от id если id кратно 3? то добавляем еще перенос строки
	console.log(id);
	var style = "bg-light";
	var new_line = "";
	if ((id % 2) == 0) {
		style = "bg-info";
	} else {
		style = "bg-warning";
	}
	
	if ((id % 3) == 0) {
		new_line = '<div class="w-100"></div>';
	} else {
		new_line = '';
	}
	
	
	
	var $root = $( ".card-deck.mt-3.justify-content-between" )
	str = '<div class="col-4"><div class="card ' + style + ' text-white mb-3" style="max-width: 20rem;"> \
  <div class="card-header">' + name + '</div> \
  <div class="card-body"> \
    <h4 class="card-title">' + email + '</h4> \
    <p class="card-text">' + comment + '</p> \
  </div> \
  </div></div>';
	html = $.parseHTML( str + new_line);
	$root.append( html );
}

});